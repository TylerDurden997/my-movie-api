# Import a module to create a model from DB
from config.database import Base
# Import the data necessary to build a tablemodel
from sqlalchemy import Column, Integer, String, Float
# Create a Model with the DB sketch
class Movie(Base):
    #Name of table
    __tablename__="movies"

    id = Column(Integer, primary_key=True)
    title = Column(String)
    overview = Column(String)
    year = Column(Integer)
    rating = Column(Float)
    category = Column(String)
# Import to create a model of movie and pass a only paraameter in a functions
from pydantic import BaseModel
# Import to do validation
from pydantic import Field
# Import a library to do the id of model and clarify when you return a list in the functions
from typing import List, Optional

# Instantiate a class an define the model of movies
# Do validation in each field 
class Movie(BaseModel):
    id: Optional[int] = None
    title: str = Field(min_length=3, max_length=20)
    overview: str = Field(min_length=15, max_length=50)
    year: int = Field(le=2022)
    rating: float = Field(ge=1, le=10.0) 
    category: str = Field(min_length=5, max_length=10)

    class Config:
        schema_extra = {
            "example":  {
                "id": 1,
                "title": "Nombre pelicula",
                "overview": "Descripcion de la pelicula",
                "year": 2022,
                "rating": 10.0,
                "category": "Suspenso" 
            }
        }
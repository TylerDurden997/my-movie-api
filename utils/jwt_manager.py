# Import library to create a token an decode the token
from jwt import encode, decode

# Define a function to receive a information to generate the token
def create_token(data: dict) -> str:
    token : str = encode(payload=data, key='my_secret_key', algorithm='HS256')
    return token

# Validate the token that it is generated in main.py
def validate_token(token: str) -> dict:
    data: dict = decode(token, key='my_secret_key', algorithms=["HS256"])
    return data
# Import the module to manage the router
from fastapi import APIRouter
# Import a module of FastAPI, Request, HTTPException
from fastapi import Depends
# Import a module to support HTML answers and Json responses
from fastapi.responses import JSONResponse
# Import Path to do validation in parameters path
from fastapi import Path
# Import Query to do validation with query parameters
from fastapi import Query
# Import a library to do the id of model and clarify when you return a list in the functions
from typing import List, Optional
# Import all paramaters of DB
from config.database import Session
# Import the model of Movie
from models.movie import Movie as MovieModel
# Import model to become a answer in jsonable_encoder
from fastapi.encoders import jsonable_encoder
# Import the module to protect the paths
from middlewares.jwt_bearer import JWTBearer
# Import to create a model of movie and pass a only paraameter in a functions
from pydantic import BaseModel
# Import to do validation
from pydantic import Field
# Import a services
from services.movie import MovieService
# Import a movie 
from schemas.movie import Movie

# Create a  specific router to manage the directions
movie_router = APIRouter()

# Create the second endpoint and tag name in documentation, protect a path with dependencies JWTBearer
@movie_router.get('/movies', tags=['movies'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
# Fuction that return in a dictionary information about the movies
def get_movies() -> List[Movie]:
    db = Session()
    result = MovieService(db).get_movies()
    # Before Services
    # Make a request to return all movies in DB
    # result = db.query(MovieModel).all()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))
    # Without DB
    # return movies
    # Return the answer as a JSON
    # return JSONResponse(status_code=200, content=movies)

# Create the third endpoint and tag name in documentation
# Here is used the path parameter
@movie_router.get('/movies/{id}', tags=['movies'], response_model=Movie, status_code=200)
# Fuction that return a specific dictionary (by id) information about the movies
def get_movies(id: int = Path(ge=1, le=4000)) -> Movie:
    db = Session()
    result = MovieService(db).get_movie(id)
    # Before services
    # Filter the database through ID parameter and return the first element that it is found
    # result = db.query(MovieModel).filter(MovieModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={"message": "Not Found"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))
    # Withour DB
    # for item in movies:
    #     if item["id"] == id:
    #         # return item
    #         return JSONResponse(content=item)
    # return JSONResponse(status_code=400, content=[])

# Create the fourth endpoint and tag name in documentation
# Here is used a query parameter
@movie_router.get('/movies/', tags=['movies'], response_model=List[Movie], status_code=200)
# Fuction that return a dictionary filtered by category and year
def get_movies_by_category(category: str = Query(min_length=5,max_length=10), year: int = Query(ge=1900, le=2022)) -> list[Movie]:
    db = Session()
    result = MovieService(db).get_movie_by_category_year(category, year)
    # Before services 
    # Get the all movies results depending of the category and year into the DB
    # result = db.query(MovieModel).filter(MovieModel.category == category, MovieModel.year == year).all()
    if not result:
        return JSONResponse(status_code=404, content={"message": "Not Found"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))
    # Without DB
    # movieFilter = [ movie if movie['category'] == category and movie['year'] == year else [] for movie in movies ]
    # return JSONResponse(status_code=200, content=movieFilter) 

# Create a fifth endpoint and tag the name in documentation
@movie_router.post('/movies', tags=['movies'], response_model=dict, status_code=201)
#def create_movie(id: int = Body(), title: str = Body(), overview: str = Body(), year: str = Body(), rating: float = Body(), category: str = Body()):
def create_movie(movie: Movie) -> dict:
    # Create a Session with DB
    db = Session()
    MovieService(db).create_movie(movie)
    # Before services
    # Deploy a new movie in a dictionary and save on variable new_movie
    # new_movie = MovieModel(**movie.dict())
    # Add new movie in DB
    # db.add(new_movie)
    # Update the DB with changes made
    # db.commit()
    # without data base
    # movies.append(movie)
    # movies.append({

    #     "id": movie.id,
    #     "title": movie.title,
    #     "overview": movie.overview,
    #     "year": movie.year,
    #     "rating": movie.rating,
    #     "category": movie.category
    #     }
    # )
    # return movies
    return JSONResponse(status_code=201, content={'message':'Pelicula agregada'})

# Create a sixth endpoint and tag name in documentations
# Function must receive a the movie id to modify
@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict, status_code=200)
# def update_movie(id: int, title: str = Body(), overview: str = Body(), year: str = Body(), rating: float = Body(), category: str = Body()):
def update_movie(id: int, film:Movie) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Not Found"})
    MovieService(db).update_movie(id, film)
    return JSONResponse(status_code=200, content={"message": "Movie update"}) 
    # Before services
    # Get the movie to modify
    # result = db.query(MovieModel).filter(MovieModel.id == id).first()
    # if not result:
    #     return JSONResponse(status_code=404, content={"message": "Not Found"})
    # Change the field to modify
    # result.title = film.title 
    # result.overview = film.overview 
    # result.year = film.year 
    # result.rating = film.rating 
    # result.category = film.category 
    # db.commit()
    # return JSONResponse(status_code=200, content={"message": "Movie update"})        
    # Without DB
    # for movie in movies:
    #     if movie["id"] == id:
    #         movie["title"] = film.title
    #         movie["overview"] = film.overview
    #         movie["year"] = film.year
    #         movie["rating"] = film.rating
    #         movie["category"] = film.category
    #         flag = True
    #     else:
    #         flag = False
    # if flag:
    #     # return movies
    #     return JSONResponse(status_code=200, content={'message':'Pelicula actualizada'})
    # else:
    #     return JSONResponse(status_code=400, content=[])
    
# Create a seventh endpoint and tag name in documentations
# Function that receive an id and delete the movie
@movie_router.delete('/movies/{id}', tags=['movies'], response_model= dict)
def delete_movie(id: int) -> dict:
    db = Session()
    result = MovieService(db).get_movie(id)
    if not result:
        return JSONResponse(status_code=404, content={"message": "Not Found"})
    
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content={'message':'Pelicula Eliminada'})

    # Before services
    # Get the movie to modify
    # result = db.query(MovieModel).filter(MovieModel.id == id).first()
    # if not result:
    #     return JSONResponse(status_code=404, content={"message": "Not Found"})
    # db.delete(result)
    # db.commit()
    # return JSONResponse(status_code=200, content={'message':'Pelicula Eliminada'})
    # Without DB
    # for movie in movies:
    #     if movie["id"] == id:
    #         movies.remove(movie)
    #         flag = True
    #     else:
    #         flag = False

    # if flag:
    #     # return movies
    #     return JSONResponse(status_code=200, content={'message':'Pelicula Eliminada'})
    # else:
    #     return JSONResponse(status_code=400, content=[])
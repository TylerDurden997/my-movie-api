# Import the module to manage the router
from fastapi import APIRouter
# Import to create a model of movie and pass a only paraameter in a functions
from pydantic import BaseModel
# Import a token from the function create_token
from utils.jwt_manager import create_token, validate_token
# Import a module to support HTML answers and Json responses
from fastapi.responses import JSONResponse
# Import a user
from schemas.user import User

user_router = APIRouter()

# Create a eighth endpoint to manage a user sutenticarion
@user_router.post('/login', tags=['auth'])
# Function taht receives a user and return the user information
def login(user: User):
    if user.email == "admin@gmail.com" and user.password == "admin":
        token: str = create_token(user.dict())
        return JSONResponse(status_code=200, content=token)
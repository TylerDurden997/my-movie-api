# Import library to protect paths in the documentatios
from fastapi.security import HTTPBearer
# Import a module of FastAPI, Request, HTTPException
from fastapi import HTTPException, Request
# Import a token from the function create_token
from utils.jwt_manager import validate_token

# Class to do protected paths
class JWTBearer(HTTPBearer):
    # Override and do asyncronus funtion the method call into the class HTTPBearer
    async def __call__(self, request: Request):
        auth = await super().__call__(request)
        data = validate_token(auth.credentials)
        if data['email'] != "admin@gmail.com":
            raise HTTPException(status_code=403, detail="Credenciales son inválidas") 
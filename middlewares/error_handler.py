# Import the module to manage the error
from starlette.middleware.base import BaseHTTPMiddleware
# Imprt the requirements to works the app FastAPI
from fastapi import FastAPI, Response, Request
# Import the module to manage JSON response
from fastapi.responses import JSONResponse

class ErrorHandler(BaseHTTPMiddleware):
    # Override the constructor with the FastAPI app
    def __init__(self, app: FastAPI) -> None:
        super().__init__(app)

    # Capture the responsa, if it is goood no problem, it is not good, show the error
    async def dispatch(self, request: Request, call_next) -> Response | JSONResponse:
        try: 
            return await call_next(request)
        except Exception as e:
            return JSONResponse(status_code=500, content={"error": str(e)})    
# Import a module of FastAPI, Request, HTTPException
from fastapi import Depends, FastAPI, HTTPException, Request
# Import a module to support HTML answers and Json responses
from fastapi.responses import HTMLResponse, JSONResponse
# Import Body to send parameters into request of POST method
from fastapi import Body
# Import Path to do validation in parameters path
from fastapi import Path
# Import Query to do validation with query parameters
from fastapi import Query
# Import to create a model of movie and pass a only paraameter in a functions
from pydantic import BaseModel
# Import to do validation
from pydantic import Field
# Import a library to do the id of model and clarify when you return a list in the functions
from typing import Optional, List
# Import a token from the function create_token
from utils.jwt_manager import create_token, validate_token
# Import all paramaters of DB
from config.database import Session, engine, Base
# Import the model of Movie
from models.movie import Movie as MovieModel
# Import model to become a answer in jsonable_encoder
from fastapi.encoders import jsonable_encoder
# Import middleware to manage error
from middlewares.error_handler import ErrorHandler
# Import the module to protect the paths
from middlewares.jwt_bearer import JWTBearer
# Import the movie router
from routers.movie import movie_router
# Import the user router
from routers.user import user_router
# Create a app with instance FastAPI
app = FastAPI()
# Configure several aspects into documentation
app.title = "Aplication in FastAPI"
app.version = "1.0.1"

# Define the middleware into the app
app.add_middleware(ErrorHandler)

# Add the router created
app.include_router(movie_router)

# Add the router created
app.include_router(user_router)

# Create a DB engine
Base.metadata.create_all(bind=engine)

# List with dictionary of movies
# movies = [
#     {
#         'id': 1,
#         'title': 'Avatar',
#         'overview': "En un exuberante planeta llamado Pandora viven los Na'vi, seres que ...",
#         'year': 2009,
#         'rating': 7.8,
#         'category': 'Acción'    
#     },
#     {
#         'id': 2,
#         'title': 'Fight Club',
#         'overview': "Tyler Durden en el confiamos",
#         'year': 1999,
#         'rating': 10.0,
#         'category': 'Filosofia'    
#     } 
# ]

# Create the firts endpoint and tag name in documentation
@app.get('/', tags = ['Home'])
# Fuction that is goiing to execute in this endpoint
def message():
    # It can return string, dictionary, html, etc
    return HTMLResponse("<h1> Hallo, Welt </h1>")


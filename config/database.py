# Import system module
import os
# Import the DB engine
from sqlalchemy import create_engine
# Import the session with the Database
from sqlalchemy.orm.session import sessionmaker
# Import a module to manage the tables on the DB
from sqlalchemy.ext.declarative import declarative_base
# Save the name of DB
sqlite_file_name = '../database.sqlite'
# Get the real path of the currently route
base_dir = os.path.dirname(os.path.realpath(__file__))
# Build the DB URL
database_url = f'sqlite:///{os.path.join(base_dir,sqlite_file_name)}'
# Define the DB engine and print it happens in console
engine = create_engine(database_url, echo=True)
# Create a session with DB engine
Session = sessionmaker(bind=engine)
# Create a instance to manage the tables on the DB
Base = declarative_base()
